

document.querySelector('.hamburger').addEventListener('click', function() {
	document.querySelector('.main-header nav').classList.toggle('open');
})

$('.hero-carousel').slick({
  slidesToShow: 6,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 1000,
  arrows: false,
  responsive: [
                {
		                breakpoint: 1204,
		                settings: {
		                slidesToShow: 4,
		                slidesToScroll: 1
	                	}
	                },
                {
		                breakpoint: 768,
		                settings: {
		                slidesToShow: 3,
		                slidesToScroll: 1
		                }
	                },
                {
		                breakpoint: 575,
		                settings: {
		                slidesToShow: 2,
		                slidesToScroll: 1
		                }
	                },
                {
		                breakpoint: 480,
		                settings: {
		                slidesToShow: 1,
		                slidesToScroll: 1
		            }
		        }
              ]
});

$(document).ready(function() {
	$('.popup-with-form').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name',

		// When elemened is focused, some mobile browsers in some cases zoom in
		// It looks not nice, so we disable it:
		callbacks: {
			beforeOpen: function() {
				if($(window).width() < 700) {
					this.st.focus = false;
				} else {
					this.st.focus = '#name';
				}
			}
		}
	});
});